# IUT Music

application Flash présentant le contenu d'une base d'albums de musique

##venv
virtualenv -p python3 venv ##créer le virtualenv
source ../venv/bin/activate     ##utiliser le venv qui est à l'éxtérieur du projet
##install
pip install flask
pip install flask_bootstrap4
pip install flask_sqlalchemy
pip install flask_login
pip install wtforms
pip install flask_wtf
pip install pyyaml



##charger la base de donner
flask loaddb ./data/extrait2.yml ##charger la base de donnée
flask loaduser ./data/utilisateur.yml ##charger les utilisateurs
flask newuser paul azerty123 ##créer un nouvelle user

#lancer le projet
flask run
http://127.0.0.1:5000/
