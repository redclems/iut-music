from src.views.views import *
from wtforms import StringField, HiddenField, PasswordField, SelectField
from flask_wtf import FlaskForm
from src.models.edit import new_user

class add_UserForm(FlaskForm):
    name = StringField('Name')
    password = PasswordField('Password')
    passwordVerif = PasswordField('Password verif')
    role = SelectField('Role')

    def get_add_user(self):
        """editer les donner de user"""
        name = self.name.data
        if self.passwordVerif.data == self.password.data:
            if get_user_exist(name) == False:
                o = new_user(name,
                             self.password.data,
                             get_role_name(self.role.data).id
                             )
                if o != False:
                    return ("The compte was created", "alert-success")
                return ("The are probleme in dataBases", "alert-success")
            return (("The user %s already exists" % (name)), "alert-danger")
        return ("The pasword is not equals", "alert-danger")

@app.route("/user/add", methods=("GET", "POST"))
def add_user():
    conection = roleConnecterAdmin()
    if conection[1]:
        f = add_UserForm()

        actionF = ""
        err = False
        f.role.choices = selectFieldListeRole(nullValue=True)
        if f.validate_on_submit():
            actionF = f.get_add_user()
            err = True
        return render_template(
                "add_user.html",
                title="IUT Music",
                connecter= conection[0],
                err = err,
                execution= actionF,
                form = f
                )
    return redirect(url_for("home"))
