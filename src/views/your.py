from src.views.views import *
from flask_login import current_user, login_required


@app.route("/compte/", methods=("GET", "POST"))
@login_required
def your():
    roleName = get_user_role(current_user.username).role
    conection = roleConnecterAdmin()
    return render_template(
        "your.html",
        title="IUT Music",
        User = current_user,
        connecter= conection[0],
        role = roleName,
        hidebp = True
        )
