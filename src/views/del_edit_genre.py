from src.views.views import *
from src.models.dellet import dellet_genre

@app.route("/genre/edit", methods=['GET', 'POST'])
def del_edit_genre():
    conection = roleConnecterAdmin()
    if conection[1]:
        return render_template(
                "del_edit.html",
                title="IUT Music",
                connecter= conection[0],
                tab = ['Genre', 'Delete'],
                Liste = get_genre(),
                modification = "Genre",
                lienAdd = "/genre/add",
                lienEdit = "#",
                lienDel = "/genre/del/"
                )
    return redirect(url_for("home"))

@app.route("/genre/del/<int:id>", methods=("GET", "POST"))
def del_genre(id):
    conection = roleConnecterAdmin()
    if conection[1]:
        genre = get_genre_id(id)
        if genre != False:
            dellet_genre(genre)
    return redirect(url_for("del_edit_genre"))
