from src.views.views import *
from wtforms import StringField, HiddenField, IntegerField, SelectMultipleField
from flask_wtf import FlaskForm
from src.models.edit import new_Playlist
from flask_login import current_user

class add_PlayListForm(FlaskForm):
    name = StringField('Name')

    def get_add_playList(self):
        """editer les donner d'un genre"""
        o = new_Playlist(current_user, self.name.data)
        if o != False:
            return (("The playlist %s was created" % (self.name.data)), "alert-success")
        return (("the playlist %s wasn't created" % (self.name.data)), "alert-danger")

@app.route("/playlist/add", methods=("GET", "POST"))
def add_playlist():
    conection = roleConnecterAdmin()
    if conection[0]:
        f = add_PlayListForm()
        actionF = ""
        err = False
        if f.validate_on_submit():
            actionF = f.get_add_playList()
            err = True
        return render_template(
                "add_playlist.html",
                title="IUT Music",
                connecter= conection[0],
                err = err,
                execution= actionF,
                form = f
                )
    return redirect(url_for("home"))
