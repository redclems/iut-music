from src.views.views import *
from src.views.views import *

@app.route("/author/<int:id>", methods=['GET', 'POST'])
def author(id):
    author = get_author_id(id)
    return render_template(
        "author.html",
        title="IUT Music",
        authors = author,
        listeAlbums = get_album_auteur(id),
        listeAlbumsP = get_album_parent(id)
        )
