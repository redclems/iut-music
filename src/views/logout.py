from src.views.views import *
from flask_login import current_user, logout_user, login_required

@app.route("/logout/", methods=("GET", "POST"))
@login_required
def logout():
        if current_user.is_authenticated:
            logout_user()
        return redirect(url_for("home"))
