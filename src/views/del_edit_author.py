from src.views.views import *
from src.models.edit import *
from src.models.dellet import dellet_author, dellet_parent
from src.models.edit import edit_author
from wtforms import StringField, HiddenField, IntegerField
from flask_wtf import FlaskForm

class edit_AuthorForm(FlaskForm):
    id = IntegerField('id')
    name = StringField('Name')

    def get_edit_author(self, id):
        """editer les donner d'un auteur"""
        return edit_author(id, self.name.data)

@app.route("/author/edit", methods=['GET', 'POST'])
def del_edit_author():
    conection = roleConnecterAdmin()
    if conection[1]:
        return render_template(
                "del_edit.html",
                title="IUT Music",
                connecter= conection[0],
                tab = ['Author', 'Edit', 'delete'],
                Liste = get_author(),
                modification = "Author",
                lienAdd = "/author/add",
                lienEdit = "/author/edit/",
                lienDel = "/author/del/"
                )
    return redirect(url_for("home"))

@app.route("/author/edit/<int:id>", methods=("GET", "POST"))
def edit_author_v(id):
    conection = roleConnecterAdmin()
    if conection[1]:
        auth = get_author_id(id)
        if auth != False:
            f = edit_AuthorForm()

            actionF = ""
            err = False
            if f.validate_on_submit():
                actionF = f.get_edit_author(id)
                err = True
            return render_template(
                    "edit_author.html",
                    title="IUT Music",
                    connecter= conection[0],
                    auth = auth,
                    err = err,
                    execution= actionF,
                    form = f
                    )
        return redirect(url_for("del_edit_author"))
    return redirect(url_for("home"))

@app.route("/author/del/<int:id>", methods=("GET", "POST"))
def del_author(id):
    conection = roleConnecterAdmin()
    if conection[1]:
        author = get_author_id(id)
        if author != False:
            dellet_author(author)
            parent = get_parent_id(id)
            if parent != False:
                dellet_parent(parent)
    return redirect(url_for("del_edit_author"))
