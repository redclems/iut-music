from src.views.views import *
from flask_login import current_user
import sqlalchemy

@app.route("/", methods=['GET', 'POST'])
def home():
    conecter = roleConnecterAdmin()

    playlist = []
    ListePlaylist = []
    if conecter[0]:
        ListePlaylist = get_playlist_user(current_user.username)

        for p in ListePlaylist:
            playlist.append(get_album_playlist_nb(p.id, 6))
        print(playlist)

    try:
        listeGenre = get_genre()
    except sqlalchemy.exc.OperationalError:
        return "Charger la base de donner", 404

    return render_template(
        "home.html",
        title="IUT Music",
        genre=listeGenre,
        albumR=get_album_recent(),
        albumMieuxNoter = get_album_mieux_noter(6)[:6],
        connecter= conecter[0],
        admin = conecter[1],
        settingAdmin= settingAdmin(),
        nP = len(playlist),
        playlist= playlist,
        ListePlaylist =ListePlaylist,
        )
