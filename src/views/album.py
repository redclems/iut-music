from src.views.views import *
from flask_login import current_user
from src.models.edit import add_album_PlayList
from src.models.new import new_Note
from wtforms import SelectField, RadioField
from flask_wtf import FlaskForm

class add_album_playlistForm(FlaskForm):
    playlist = SelectField('playlist')

    def get_add_album(self, id_alb):
        """editer les donner de user"""

        return add_album_PlayList(
                        self.playlist.data,
                        id_alb
                        )

class add_noteForm(FlaskForm):
    note = RadioField('note')


    def add_note(self, id_alb):
        """editer les donner de user"""
        o = get_Note_user(current_user.username, id_alb)
        if o == False:
            note = new_Note(current_user, self.note.data, id_alb)
            if note == False:
                return ("An error occurred when you are noting", "alert-danger")
            return ("The note was add to the album thank you for noting", "alert-success")
        return ("You are already noted", "alert-danger")

@app.route("/album/<int:id>", methods=['GET', 'POST'])
def album(id):
    album = get_album(id)
    genre = get_genre_album(id)
    actionF = ""
    err = False
    conection = roleConnecterAdmin()
    spotify = False

    if album.url != None and "spotify" in album.url and 'embed' in album.url:
        spotify = True

    f2 = add_noteForm()
    f2.note.choices = [("1", "1"), ("2", "2"), ("3", "3"), ("4", "4"), ("5", "5")]
    noteM = get_list_noteM_album(id)
    if noteM == False:
        noteM = "aucune"
    else:
        noteM = round(noteM, 1)

    if conection[0]:
            f = add_album_playlistForm()
            f.playlist.choices = selectFieldListePlaylist(current_user.username, True)

            if f.validate_on_submit():
                actionF = f.get_add_album(id)
                err = True

            if f2.validate_on_submit():
                actionF = f2.add_note(id)
                err = True
            note = get_note_user(current_user.username, id)
            if note == False:
                note = ""

            return render_template(
                "album.html",
                title = "IUT Music",
                spotify=spotify,
                album = album,
                listGenre=get_genre_album(id),
                connecter= conection[0],
                execution= actionF,
                err = err,
                form = f,
                form2 = f2,
                note=note,
                noteM = noteM
                )
    return render_template(
        "album.html",
        title = "IUT Music",
        spotify=spotify,
        album = album,
        connecter= conection[0],
        listGenre=get_genre_album(id),
        form2 = f2,
        noteM = noteM
        )
