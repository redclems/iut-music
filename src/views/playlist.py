from src.views.views import *
from flask_login import current_user
from src.models.dellet import dellet_album_to_playlist

@app.route("/playlist/<int:id>", methods=['GET', 'POST'])
def playlist(id):
    """voirs tous les albums d'une playlist"""
    #res du get
    page = request.args.get('page')
    order = request.args.get('order')

    conecter = roleConnecterAdmin()

    #trier oui ou non
    if order == "years":
        r = [("years", "growing year"), ("yearsD", "year descending"), ("","pas d'ordre")]
    elif order == "yearsD":
        r = [("yearsD", "year descending"), ("years", "growing year"), ("","pas d'ordre")]
    else:
        r = [("","order by"), ("years", "growing year"), ("yearsD", "year descending")]
        order = None

    #page parametre
    if page == None:
        page = 1

    #recherche
    dataGET = ""

    if order != None:
        dataGET = 'order=' + order + '&'
        listeAlbum = get_album_playlist(id, order)
    else:
        listeAlbum = get_album_playlist(id)

    playlist = get_playlist_exist(id)

    user_playlist = get_user_playlist(id)

    #droit pour modifier la playlist
    droit = False
    userHere = False
    if len(user_playlist) != 0:
        userHere = True
        if current_user in user_playlist:
            droit = True
        user_playlist = user_playlist[0]
    else:
        user_playlist = ""

    #pagination
    listeVide=False

    pageMax = len(listeAlbum)/12
    pagination = (int(page), int(page)-1, int(page)+1, int(pageMax) + (not pageMax.is_integer()))
    debut = int((int(page)-1)*12)# position album debut de la page
    max = int(int(page)*12)# position album fin de la page
    listeAlbum = listeAlbum[debut:max]

    if len(listeAlbum) == 0 or playlist == False:
        listeVide = True

    return render_template(
        "playlist.html",
        title="IUT Music",
        album=listeAlbum,
        genre=get_genre(),
        page=pagination,
        user_playlist= user_playlist,
        GET=dataGET,
        playlist=playlist,
        connecter= conecter[0],
        admin = conecter[1],
        droitModif = droit,
        settingAdmin= settingAdmin(),
        notFound=listeVide,
        ranger=r,
        id=id
        )

@app.route("/playlist/<int:id>/del", methods=("GET", "POST"))
def del_album_to_playlist(id):
    conection = roleConnecterAdmin()
    if conection[0]:
        id_album = request.args.get('id_album')
        if id_album != None:
            album = get_album_exist(id_album)
            if album != False:
                #droit pour modifier la playlist
                user_playlist = get_user_playlist(id)
                if user_playlist != False:
                    if current_user in user_playlist:
                        playlist = get_playlist_exist(id)
                        if playlist != False:
                            dellet_album_to_playlist(album, playlist)
    return redirect(url_for("playlist", id=id))
