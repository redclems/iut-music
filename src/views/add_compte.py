from src.views.views import *
from wtforms import StringField, PasswordField
from flask_wtf import FlaskForm
from src.models.edit import new_user

class add_CompteForm(FlaskForm):
    name = StringField('Name')
    password = PasswordField('Password')
    passwordVerif = PasswordField('Password verif')

    def get_add_user(self):
        """editer les donner de user"""
        name = self.name.data
        if self.passwordVerif.data == self.password.data:
            if get_user_exist(name) == False:
                o = new_user(name,
                             self.password.data
                             )
                if o != False:
                    return ("Your compte was created", "alert-success")
                return ("The are probleme in dataBases", "alert-success")
            return (("The user %s already exists" % (name)), "alert-danger")
        return ("The pasword is not equals", "alert-danger")

@app.route("/user/compte", methods=("GET", "POST"))
def add_compte():
    conection = roleConnecterAdmin()
    if conection[0] == False:
        f = add_CompteForm()

        actionF = ""
        err = False
        if f.validate_on_submit():
            actionF = f.get_add_user()
            err = True
        return render_template(
                "add_compte.html",
                title="IUT Music",
                connecter= conection[0],
                err = err,
                execution= actionF,
                form = f
                )
    return redirect(url_for("home"))
