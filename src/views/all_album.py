from src.views.views import *
import sqlalchemy

@app.route("/album", methods=['GET', 'POST'])
def all_album():
    """voirs tous les albums"""
    #res du get
    page = request.args.get('page')
    order = request.args.get('order')
    recherche = request.args.get('search')


    dataGET = ""
    conecter = roleConnecterAdmin()

    #trier oui ou non
    if order == "years":
        r = [("years", "growing year"), ("yearsD", "year descending"), ("note", "note year"),("noteD", "note descending"), ("","pas d'ordre")]
    elif order == "yearsD":
        r = [("yearsD", "year descending"), ("years", "growing year"), ("note", "note year"),("noteD", "note descending"), ("","pas d'ordre")]
    elif order == "note":
        r = [("note", "note year"),("noteD", "note descending"), ("yearsD", "year descending"), ("years", "growing year"), ("","pas d'ordre")]
    elif order == "noteD":
        r = [("noteD", "note descending"), ("note", "note year"), ("yearsD", "year descending"), ("years", "growing year"), ("","pas d'ordre")]
    else:
        r = [("","order by"), ("years", "growing year"), ("yearsD", "year descending"),("noteD", "note descending"), ("note", "note year")]
        order = None

    #page parametre
    if page == None:
        page = 1

    #recherche
    if recherche == None:

        try:
            listeAlbum = get_sample(order)
        except sqlalchemy.exc.OperationalError:
            return "Charger la base de donner", 404

        if order != None:
            dataGET = 'order=' + order + '&'
    else:
        listeAlbum = get_sample_found(recherche, order)
        if order != None:
            dataGET = 'search='+ recherche + '&' + 'order=' + order + '&'
        else:
            dataGET = 'search='+ recherche + '&'

    #agination
    pageMax = len(listeAlbum)/12
    pagination = (int(page), int(page)-1, int(page)+1, int(pageMax) + (not pageMax.is_integer()))
    debut = int((int(page)-1)*12)# position album debut de la page
    max = int(int(page)*12)# position album fin de la page
    listeAlbum = listeAlbum[debut:max]
    listeVide=False

    if len(listeAlbum) == 0:
        listeVide = True

    return render_template(
        "all_album.html",
        title="IUT Music",
        album=listeAlbum,
        genre=get_genre(),
        page=pagination,
        GET=dataGET,
        connecter= conecter[0],
        admin = conecter[1],
        settingAdmin= settingAdmin(),
        notFound=listeVide,
        ranger=r,
        id=0
        )
