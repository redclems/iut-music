from src.views.views import *
from wtforms import StringField, HiddenField, IntegerField, SelectMultipleField
from flask_wtf import FlaskForm
from src.models.edit import new_genre

class add_GenreForm(FlaskForm):
    name = StringField('Name')

    def get_add_genre(self):
        """editer les donner d'un genre"""
        o = new_genre(self.name.data)
        if o != False:
            return (("The author %s was created" % (self.name.data)), "alert-success")
        return (("The author %s wasn't created" % (self.name.data)), "alert-danger")

@app.route("/genre/add", methods=("GET", "POST"))
def add_author_oi():
    conection = roleConnecterAdmin()
    if conection[1]:
        f = add_GenreForm()
        actionF = ""
        err = False
        if f.validate_on_submit():
            actionF = f.get_add_genre()
            err = True
        return render_template(
                "add_genre.html",
                title="IUT Music",
                connecter= conection[0],
                err = err,
                execution= actionF,
                form = f
                )
    return redirect(url_for("home"))
