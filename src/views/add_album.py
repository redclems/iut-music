from src.views.views import *
from src.models.edit import new_album, new_author, new_parent
from wtforms import StringField, HiddenField, IntegerField, SelectMultipleField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_wtf import FlaskForm
from werkzeug.utils import secure_filename
import os

class add_AlbumForm(FlaskForm):
    id = IntegerField('id')
    title = StringField('title')
    entryId = IntegerField('entryId')
    img =  FileField("image", validators=[FileAllowed(['jpeg'])])
    releaseYear = IntegerField('releaseYear')
    url = StringField('url')
    author = StringField('author')
    parent = StringField('parent')
    genres = SelectMultipleField('genre')

    def get_add_album(self):
        """editer les donner de album"""
        ListeauteurId = get_auteur_name(self.author.data)
        ListeparentId = get_parent_name(self.parent.data)

        image = self.img.data
        filename = "null"
        if image != None:
            filename = self.title.data+".jpeg"

            image.save(os.path.join(
                app.static_folder, 'album_img', filename
            ))

        if len(ListeauteurId) == 0:
            ListeauteurId.append(new_author(self.author.data))
            new_parent(ListeauteurId[0].id, self.author.data)

        if len(ListeparentId) == 0:
            p = self.parent.data
            auth = None
            listeAUTH = get_auteur_name(p)
            if len(listeAUTH) == 0:
                listeAUTH.append(new_author(p))

            new = new_parent(listeAUTH[0].id, p)
            ListeparentId.append(new)

        auteurId = ListeauteurId[0].id
        parentId = ListeparentId[0].id
        o =  new_album(
                    self.entryId.data,
                    self.title.data,
                    filename,
                    self.releaseYear.data,
                    self.url.data,
                    auteurId,
                    parentId,
                    self.genres.data)
        if o != False:
            return (("The album %s was created" % (self.title.data)), "alert-success")
        return (("The album %s wasn't created" % (self.title.data)), "alert-danger")

@app.route("/album/add", methods=("GET", "POST"))
def add_album():
    conection = roleConnecterAdmin()
    if conection[1]:
        f = add_AlbumForm()
        f.genres.choices = selectFieldListeGenre()
        actionF = ""
        err = False
        if f.validate_on_submit():
            actionF = f.get_add_album()
            err = True
        return render_template(
                "add_album.html",
                title="IUT Music",
                connecter= conection[0],
                err = err,
                execution= actionF,
                form = f
                )
    return redirect(url_for("home"))
