"""
from wtforms import StringField, HiddenField, IntegerField, RadioField, SelectMultipleField, PasswordField, SelectField
from flask_login import login_user, current_user, logout_user, login_required
from flask_wtf import FlaskForm
from src.models.edit import *
"""
from src.models.models import *
from src.models.get import *

from flask_login import current_user
from flask import url_for, redirect, request, render_template
from src.app import app, db



def roleConnecterAdmin():
    user_connecter = False
    admin = False
    if current_user.is_authenticated:
        user_connecter = True
        if get_user_role(current_user.username).role == "admin":
            admin = True

    return(user_connecter, admin)

def settingAdmin():
    return [
       ("user/edit", "edit user"),
       ("album/edit", "edit album"),
       ("author/edit", "edit author"),
       ("genre/edit", "edit genre")]
