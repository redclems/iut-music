from src.views.views import *
from wtforms import StringField, HiddenField, IntegerField, SelectMultipleField
from flask_wtf import FlaskForm
from src.models.edit import new_author

class add_AuthorForm(FlaskForm):
    name = StringField('Name')

    def get_add_author(self):
        """editer les donner d'un auteur"""
        o = new_author(self.name.data)
        if o != False:
            return (("The author %s was created" % (self.name.data)), "alert-success")
        return (("The author %s wasn't created" % (self.name.data)), "alert-danger")

@app.route("/author/add", methods=("GET", "POST"))
def add_author():
    conection = roleConnecterAdmin()
    if conection[1]:
        f = add_AuthorForm()
        actionF = ""
        err = False
        if f.validate_on_submit():
            actionF = f.get_add_author()
            err = True
        return render_template(
                "add_author.html",
                title="IUT Music",
                connecter= conection[0],
                err = err,
                execution= actionF,
                form = f
                )
    return redirect(url_for("home"))
