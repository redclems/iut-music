from src.views.views import *


@app.errorhandler(404)
def page_404(e):
    return render_template(
        "page_404.html",
        title="IUT Music"
        ), 404
