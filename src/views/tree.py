from src.views.views import *

@app.route("/tree")
def tree():
    listeLien = ["/album","/author","/user","/logout/","/login/","/genre/add","/user/add","/album/add","/author/add","/user/compte","/genre/edit","/author/edit","/album/edit"]
    return render_template(
        "tree.html",
        title = "IUT Music",
        listeLien = listeLien
        )
