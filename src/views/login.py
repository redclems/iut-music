from src.views.views import *
from flask_login import login_user, current_user, logout_user, login_required
from wtforms import StringField, PasswordField
from flask_wtf import FlaskForm

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')

    def get_authenticated_user(self):
        user = get_user_exist(self.username.data)
        if user == False:
            return False
        passwd = codePasword(self.password.data)
        return user if passwd == user.password else False

@app.route("/login/", methods=("GET", "POST"))
def login():
    conection = roleConnecterAdmin()
    if conection[0] == False:
        f = LoginForm()
        erreur = False
        if f.validate_on_submit():
            user = f.get_authenticated_user()
            if user:
                login_user(user)
                return redirect(url_for("home"))
            else:
                erreur = True

        return render_template(
            "login.html",
            title="IUT Music",
            err = erreur,
            form = f,
            hidebp = True)
    return redirect(url_for("home"))
