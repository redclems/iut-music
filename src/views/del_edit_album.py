from src.views.views import *
from src.models.edit import *
from src.models.dellet import dellet_album
from src.models.edit import edit_album
from wtforms import StringField, HiddenField, IntegerField, RadioField, SelectMultipleField, PasswordField, SelectField
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from werkzeug.utils import secure_filename
import os

class edit_AlbumForm(FlaskForm):
    id = IntegerField('id')
    title = StringField('title')
    entryId = IntegerField('entryId')
    img =  FileField("image", validators=[FileAllowed(['jpeg'])])
    releaseYear = IntegerField('releaseYear')
    url = StringField('url')
    author = StringField('author')
    parent = StringField('parent')
    genres = SelectMultipleField('genre')

    def get_edit_album(self, id):
        """editer les donner de album"""
        auteurId = get_auteur_album(id).id
        print(get_auteur_album(id).name)
        parentId = get_parent_album(id).id
        print(get_parent_album(id).name)


        image = self.img.data
        filename = "null"
        if image != None:
            filename = self.title.data+".jpeg"

            image.save(os.path.join(
                app.static_folder, 'album_img', filename
            ))

        return edit_album(
                    id,
                    self.entryId.data,
                    self.title.data,
                    filename,
                    self.releaseYear.data,
                    self.url.data,
                    auteurId,
                    parentId,
                    self.genres.data)


@app.route("/album/edit", methods=['GET', 'POST'])
def del_edit_album():
    conection = roleConnecterAdmin()
    if conection[1]:
        return render_template(
                "del_edit.html",
                title="IUT Music",
                connecter= conection[0],
                tab = ['Album', 'Edit', 'Delete'],
                Liste = get_albums(),
                modification = "Album",
                lienAdd = "/album/add",
                lienEdit = "/album/edit/",
                lienDel = "/album/del/"
                )
    return redirect(url_for("home"))

@app.route("/album/edit/<int:id>", methods=("GET", "POST"))
def edit_album_v(id):
    conection = roleConnecterAdmin()
    if conection[1]:
        alb = get_album_exist(id)
        if alb != False:
            f = edit_AlbumForm()
            f.genres.choices = selectFieldListeGenre()

            actionF = ""
            err = False
            if f.validate_on_submit():
                actionF = f.get_edit_album(id)
                err = True
            return render_template(
                    "edit_album.html",
                    title="IUT Music",
                    connecter= conection[0],
                    alb = alb,
                    err = err,
                    execution= actionF,
                    form = f
                    )
        return redirect(url_for("del_edit_album"))
    return redirect(url_for("home"))

@app.route("/album/del/<int:id>", methods=("GET", "POST"))
def del_album(id):
    conection = roleConnecterAdmin()
    if conection[1]:
        album = get_album_exist(id)
        if album != False:
            dellet_album(album)
    return redirect(url_for("del_edit_album"))
