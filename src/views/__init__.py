import src.views.views
import src.views.page_404
import src.views.home
import src.views.all_album

import src.views.album
import src.views.del_edit_album
import src.views.add_album

import src.views.author
import src.views.del_edit_author
import src.views.add_author

import src.views.del_edit_genre
import src.views.add_genre

import src.views.your
import src.views.del_edit_user
import src.views.add_user

import src.views.login
import src.views.logout
import src.views.add_compte

import src.views.tree

import src.views.playlist
import src.views.add_playlist
