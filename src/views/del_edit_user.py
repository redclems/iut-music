from src.views.views import *
from src.models.dellet import dellet_user
from src.models.edit import edit_user
from wtforms import StringField, HiddenField, PasswordField, SelectField
from flask_wtf import FlaskForm

class edit_UserForm(FlaskForm):
    name = StringField('Name')
    password = PasswordField('Password')
    role = SelectField('Role')

    def get_edit_user(self, name):
        """editer les donner de user"""
        role = self.role.data

        return edit_user(
                        name,
                        self.password.data,
                        role
                        )

@app.route("/user/edit", methods=['GET', 'POST'])
def del_edit_user():
    conection = roleConnecterAdmin()
    if conection[1]:
        return render_template(
                "del_edit.html",
                title="IUT Music",
                connecter= conection[0],
                tab = ['User', 'Edit', 'Delete'],
                Liste = get_user(),
                modification = "User",
                lienAdd = "/user/add",
                lienEdit = "/user/edit/",
                lienDel = "/user/del/"
                )
    return redirect(url_for("home"))

@app.route("/user/edit/<string:name>", methods=("GET", "POST"))
def edit_user_v(name):
    conection = roleConnecterAdmin()
    if conection[1]:
        user = get_user_exist(name)
        if user != False:
            f = edit_UserForm()

            actionF = ""
            err = False
            f.role.choices = selectFieldListeRole(startWith=user.role.role)
            if f.validate_on_submit():
                actionF = f.get_edit_user(name)
                err = True
            return render_template(
                    "edit_user.html",
                    title="IUT Music",
                    connecter= conection[0],
                    user = user,
                    err = err,
                    execution= actionF,
                    form = f
                    )
        return redirect(url_for("del_edit_user"))
    return redirect(url_for("home"))

@app.route("/user/del/<string:name>", methods=("GET", "POST"))
def del_user(name):
    conection = roleConnecterAdmin()
    if conection[1]:
        user = get_user_exist(name)
        if user != False:
            dellet_user(user)
    return redirect(url_for("del_edit_user"))
