from flask import Flask
from flask_bootstrap import Bootstrap
import os.path

def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))

from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)


app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../iut_music.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "BED8C4E7-305D-4792-8FEA-8D1E2E863E79"

from flask_login import LoginManager
login_manager = LoginManager(app)
