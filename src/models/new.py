from src.app import db
from sqlalchemy import Table, Column, Integer, ForeignKey, Date, or_, desc, func, select
from sqlalchemy.orm import relationship
from src.models.models import *
from datetime import datetime
from src.models.get import *

def commit():
    db.session.commit()

def new_role(name):
    o = get_role_exist(name)
    if o == False:
        o = Role(role=name)
        db.session.add(o)
        commit()
        return o
    return False

def new_user(username, password, role_id=None):
    if role_id == None:
        role_id = get_role_name("utilisateur").id
    if role_id == False:
        return False
    o = User(username=username,
            password=codePasword(password),
            role_id=role_id)
    db.session.add(o)
    commit()
    return o

def new_parent(idAuthor, name):
    o = Parent(id=idAuthor, name=name)
    db.session.add(o)
    commit()
    return o

def new_author(name):
    o = get_author_exist(name)
    if o == False:
        o = Author(name=name)
        db.session.add(o)
        commit()
        return o
    return False

def new_genre(name):
    o = get_genre_exist(name)
    if o == False:
        o = Genre(name=name.lower())
        db.session.add(o)
        commit()
        return o
    return False

def new_album(entryId, title, img, releaseYear, url, idAuthor, idParent, listGenre):
    o = get_albums_title_AuthorId(title, idAuthor)
    if len(o) == 0:
        if img == None or img == 'null' or img == ''  or img == ' ':
            img = "none.png"
        o = Album(entryId = entryId,
                    img = img,
                    releaseYear = releaseYear,
                    title = title,
                    url = url,
                    author_id = idAuthor,
                    parent_id = idParent
                    )
        #ajout des genre
        for g in listGenre:
            g = g.lower()
            gFound = get_genre_exist(g)
            if gFound == False:
                gFound = new_genre(g)
            o.genres.append(gFound)
        db.session.add(o)
        commit()
        return o
    return False

def add_History(album, user, date=datetime.now()):
    o = History(album_id = album.id,
                user_name = user.username,
                date=date
                )
    db.session.add(o)
    commit()
    return o

def new_Playlist(user, name, listeMusic=[]):
    o = Playlist(name = name)
    #ajout des albums
    for album in listeMusic:
        o.album.append(album)
    db.session.add(o)
    user.playlist.append(o)
    commit()
    return o

def new_Note(user, note, id_album):
    o = get_Note_user(user.username, id_album)
    if o == False:
        o = Note(note = int(note))
        #ajout des albums
        alb = get_album_exist(id_album)
        if alb != False:
            o.album.append(alb)
        else:
            return False
        db.session.add(o)
        user.note.append(o)
        commit()
        return o
    return o[0]
