from src.app import db
from sqlalchemy import Table, Column, Integer, ForeignKey, Date, or_, desc, func, select
from sqlalchemy.orm import relationship
from hashlib import sha256

Style_table = Table('Style', db.Model.metadata,
    Column('Album_id', ForeignKey('Album.id'), primary_key=True),
    Column('Genre_id', ForeignKey('Genre.id'), primary_key=True)
    )
PlayList_album = Table('PlayList_album', db.Model.metadata,
    Column('Album_id', ForeignKey('Album.id'), primary_key=True),
    Column('Playlist_id', ForeignKey('Playlist.id'), primary_key=True)
    )

PlayList_user = Table('PlayList_user', db.Model.metadata,
    Column('Username', ForeignKey('User.username'), primary_key=True),
    Column('Playlist_id', ForeignKey('Playlist.id'), primary_key=True)
    )

Note_table = Table('NoteAlbum', db.Model.metadata,
    Column('Note_id', ForeignKey('Note.id'), primary_key=True),
    Column('Album_id', ForeignKey('Album.id'), primary_key=True)
    )

Note_user = Table('NoteUser', db.Model.metadata,
    Column('Username', ForeignKey('User.username'), primary_key=True),
    Column('Note_id', ForeignKey('Note.id'), primary_key=True)
    )

#-user model of data
class Genre(db.Model):
    __tablename__ = 'Genre'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    albums = relationship("Album",
        secondary=Style_table,
        back_populates="genres")

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<Genre (%d) %s>" % (self.id, self.name)

class Note(db.Model):
    __tablename__ = 'Note'
    id = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.Integer)
    user = relationship("User",
        secondary=Note_user,
        back_populates="note")
    album = relationship("Album",
        secondary=Note_table,
        back_populates="note")

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<Genre (%d) %s>" % (self.id, self.name)

class Author(db.Model):
    __tablename__ = 'Author'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<Author (%d) %s>" % (self.id, self.name)

class Parent(db.Model):
    __tablename__ = 'Parent'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<Parent (%d) %s>" % (self.id, self.name)



class Album(db.Model):
    __tablename__ = 'Album'
    id = db.Column(db.Integer, primary_key=True)
    entryId = db.Column(db.Integer)
    img = db.Column(db.String(100))
    releaseYear = db.Column(db.Integer)
    title = db.Column(db.String(40))
    url = db.Column(db.String(100))
    author_id = db.Column(db.Integer, db.ForeignKey("Author.id"))
    author = db.relationship("Author",
        backref=db.backref("album", lazy = "dynamic"))
    parent_id = db.Column(db.Integer, db.ForeignKey("Parent.id"))
    parent = db.relationship("Parent",
        backref=db.backref("albumParent", lazy = "dynamic"))
    genres = relationship("Genre",
        secondary=Style_table,
        back_populates="albums")
    playlist = relationship("Playlist",
        secondary=PlayList_album,
        back_populates="album")
    note = relationship("Note",
        secondary=Note_table,
        back_populates="album")

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<Album (%d) %s>" % (self.id, self.title)

class Role(db.Model):
    __tablename__ = 'Role'
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(50))

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<Role (%d) %s>" % (self.id, self.role)

class History(db.Model):
    __tablename__ = 'History'
    user_name = db.Column(db.String(50), db.ForeignKey("User.username"), primary_key=True)
    user = db.relationship("User",
        backref=db.backref("user_Listen", lazy = "dynamic"))
    album_id = db.Column(db.Integer, db.ForeignKey("Album.id"), primary_key=True)
    album = db.relationship("Album",
        backref=db.backref("album_Listen", lazy = "dynamic"))
    date = db.Column(db.DateTime)

    def __repr__(self):
        return "<History %s %s>" % (self.user_name, self.album_id)

from flask_login import UserMixin

class Playlist(db.Model):
    __tablename__ = 'Playlist'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    album = relationship("Album",
            secondary=PlayList_album,
            back_populates="playlist")
    user = relationship("User",
            secondary=PlayList_user,
            back_populates="playlist")

    def get_id(self):
        return self.id

    def __repr__(self):
        return "<Playlist (%d) %s>" % (self.id, self.name)

class User(db.Model, UserMixin ):
    __tablename__ = 'User'
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(100))
    role_id = db.Column(db.Integer, db.ForeignKey("Role.id"))
    role = db.relationship("Role",
        backref=db.backref("user", lazy = "dynamic"))
    playlist = db.relationship("Playlist",
            secondary=PlayList_user,
            back_populates="user")
    note = db.relationship("Note",
            secondary=Note_user,
            back_populates="user")

    def get_id(self):
        return self.username

    def __repr__(self):
        return "<User %s>" % (self.username)

def codePasword(pasword):
    m = sha256()
    m.update(pasword.encode())
    return m.hexdigest()

from src.app import login_manager
@login_manager.user_loader
def load_user(username):
     return User.query.get(username)
