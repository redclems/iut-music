from src.app import db
from sqlalchemy import Table, Column, Integer, ForeignKey, Date, or_, desc, func, select
from sqlalchemy.orm import relationship
from src.models.models import *
import sqlalchemy
from statistics import mean

def get_sample(order=None):
    if order == None:
        return Album.query.all()
    if order == "years":
        return Album.query.order_by(Album.releaseYear).all()
    if order == "yearsD":
        return Album.query.order_by(desc(Album.releaseYear)).all()
    if order == "note":
        return Album.query.join(Note_table).join(Note).order_by(Note.note).all()
    if order == "noteD":
        return Album.query.join(Note_table).join(Note).order_by(desc(Note.note)).all()



def get_sample_found(found, order=None):
    if found != "":
        if order == None:
            return Album.query.join(Style_table).join(Genre).join(Author).join(Parent).filter(or_(
                            ((Style_table.c.Album_id == Album.id) & (Style_table.c.Genre_id == Genre.id) & (Genre.name == found)),
                            (Album.releaseYear == found), (Parent.name == found), (Author.name == found), (Album.title == found)
                            )).all()
        if order == "years":
            return Album.query.join(Style_table).join(Genre).join(Author).join(Parent).filter(or_(
                            ((Style_table.c.Album_id == Album.id) & (Style_table.c.Genre_id == Genre.id) & (Genre.name == found)),
                            (Album.releaseYear == found), (Parent.name == found), (Author.name == found), (Album.title == found)
                            )).order_by(Album.releaseYear).all()
        if order == "yearsD":
            return Album.query.join(Style_table).join(Genre).join(Author).join(Parent).filter(or_(
                            ((Style_table.c.Album_id == Album.id) & (Style_table.c.Genre_id == Genre.id) & (Genre.name == found)),
                            (Album.releaseYear == found), (Parent.name == found), (Author.name == found), (Album.title == found)
                            )).order_by(desc(Album.releaseYear)).all()
        if order == "note":
            return Album.query.join(Style_table).join(Genre).join(Author).join(Parent).join(Note_table).join(Note).filter(or_(
                            ((Style_table.c.Album_id == Album.id) & (Style_table.c.Genre_id == Genre.id) & (Genre.name == found)),
                            (Album.releaseYear == found), (Parent.name == found), (Author.name == found), (Album.title == found)
                            )).order_by(Note.note).all()
        if order == "noteD":
            return Album.query.join(Style_table).join(Genre).join(Author).join(Parent).join(Note_table).join(Note).filter(or_(
                            ((Style_table.c.Album_id == Album.id) & (Style_table.c.Genre_id == Genre.id) & (Genre.name == found)),
                            (Album.releaseYear == found), (Parent.name == found), (Author.name == found), (Album.title == found)
                            )).order_by(desc(Note.note)).all()

    else:
        return get_sample(order)

def get_sample_genre(size, id_Genre):
    return Album.query.join(Style_table).join(Genre).filter((Style_table.c.Album_id == Album.id) & (Style_table.c.Genre_id == id_Genre)).all()


def get_album_mieux_noter(nb):
    return Album.query.join(Note_table).join(Note).order_by(Note.note).all()

def get_genre_album(idAlbum):
    return Genre.query.join(Style_table).join(Album).filter(Style_table.c.Album_id == idAlbum).all()

def get_album_playlist(idPlayList, order=None):
    if order == None:
        return Album.query.join(PlayList_album).join(Playlist).filter(PlayList_album.c.Playlist_id == idPlayList).all()
    if order == "years":
        return Album.query.join(PlayList_album).join(Playlist).filter(PlayList_album.c.Playlist_id == idPlayList).order_by(Album.releaseYear).all()
    if order == "yearsD":
        return Album.query.join(PlayList_album).join(Playlist).filter(PlayList_album.c.Playlist_id == idPlayList).order_by(desc(Album.releaseYear)).all()


def get_album_playlist_nb(idPlayList, nbalbum):
    return Album.query.join(PlayList_album).join(Playlist).filter(PlayList_album.c.Playlist_id == idPlayList).limit(nbalbum)

def get_user_playlist(idPlayList):
    return User.query.join(PlayList_user).join(Playlist).filter(PlayList_user.c.Playlist_id == idPlayList).all()


def get_album_in_playlist(idPlayList, idAlbum):
    return Album.query.join(PlayList_album).join(Playlist).filter((PlayList_album.c.Playlist_id == idPlayList) & (PlayList_album.c.Album_id == idAlbum) ).all()

def get_playlist_user(User_name):
    return Playlist.query.join(PlayList_user).join(User).filter(PlayList_user.c.Username == User_name).all()

def get_genre_name(name):
    return Genre.query.filter(Genre.name==name).all()

def get_album(id):
    return Album.query.get_or_404(id)

def get_album_recent():
    return Album.query.order_by(desc(Album.releaseYear)).limit(6)

def get_album_most_listen():
    return db.session.query(Album, func.count(History.album_id).label('total')).join(History).group_by(Album).order_by('total')

def get_album_listen_last(nameUser):
    return Album.query.join(History).join(User).filter(History.user_name == nameUser).order_by(History.date).limit(6)

def get_albums():
    return Album.query.all()

def get_author(id):
    return Author.query.get_or_404(id)

def get_authors():
    return Author.query.all()

def get_parent():
    return Parent.query.all()

def get_album_author(idA):
    return Author.query.filter(Author.id==idA).one().album.filter().all()

def get_auteur_name(name):
    return Author.query.filter(Author.name==name).all()

def get_parent_name(name):
    return Parent.query.filter(Parent.name==name).all()

def get_albums_title_entryId(title, entryId):
    return Album.query.filter((Album.title==title) & (Album.entryId==entryId)).all()

def get_albums_title_AuthorId(title, IdAuthor):
    return Album.query.filter((Album.title==title) & (Album.author_id==IdAuthor)).all()

def get_album_auteur(idA):
    """en fonction de l'id auteur"""
    return Author.query.filter(Author.id==idA).one().album.filter().all()

def get_album_parent(idA):
    """en fonction de l'id auteur"""
    try:
        return Parent.query.filter(Parent.id==idA).one().albumParent.filter(Album.author_id != Album.parent_id).all()
    except sqlalchemy.exc.NoResultFound:
        return []

def get_auteur_album(idAlb):
    """en fonction de l'id album"""
    o = Author.query.join(Album).filter(Album.id == idAlb).all()
    if len(o) == 0:
        return False
    return o[0]

def get_parent_album(idAlb):
    """en fonction de l'id album"""
    o = Parent.query.join(Album).filter(Album.id == idAlb).all()
    if len(o) == 0:
        return False
    return o[0]

def get_genre():
    return Genre.query.all()

def get_role():
    return Role.query.all()

def get_user():
    return User.query.all()

def get_author():
    return Author.query.all()

def get_role_name(name):
    role = Role.query.filter(Role.role==name).all()
    if len(role) == 0:
        return False
    return role[0]

def get_role_id_name(name):
    role = Role.query.filter(Role.role==name).one()
    return role.id

def get_author_id(id):
    Auth = Author.query.filter(Author.id == id).all()
    if len(Auth) == 0:
        return False
    return Auth[0]

def get_parent_id(id):
    auth = Parent.query.filter(Parent.id == id).all()
    if len(auth) == 0:
        return False
    return auth[0]

def get_genre_id(id):
    ge = Genre.query.get(id)
    if ge == None:
        return False
    return ge

def get_album_exist(id):
    alb = Album.query.filter(Album.id == id).all()
    if len(alb) == 0:
        return False
    return alb[0]

def get_user_role(name):
    role = Role.query.join(User).filter(User.username == name).all()
    if len(role) == 0:
        return False
    return role[0]

def get_role_exist(name):
    role = Role.query.filter(Role.role == name).all()
    if len(role) == 0:
        return False
    return role[0]

def get_genre_exist(name):
    ge = Genre.query.filter(Genre.name == name).all()
    if len(ge) == 0:
        return False
    return ge[0]

def get_author_exist(name):
    Auth = Author.query.filter(Author.name == name).all()
    if len(Auth) == 0:
        return False
    return Auth[0]

def get_parent_exist(name):
    par = Parent.query.join(Author).filter(Author.name == name).all()
    if len(par) == 0:
        return False
    return par[0]

def get_user_exist(user_name):
    user = User.query.filter(User.username == user_name).all()
    if len(user) == 0:
        return False
    return user[0]

def get_playlist_exist(id):
    play = Playlist.query.filter(Playlist.id == id).all()
    if len(play) == 0:
        return False
    return play[0]

def get_list_note_user(User_name):
     return Note.query.join(Note_user).join(User).filter(Note_user.c.Username == User_name).all()

def get_list_note_album(id_album):
     return Note.query.join(Note_table).join(Album).filter(Note_table.c.Album_id == id_album).all()

def get_list_noteM_album(id_album):
     listeNote = Note.query.join(Note_table).join(Album).filter(Note_table.c.Album_id == id_album).all()
     if len(listeNote) != 0:
         res = 0
         for note in listeNote:
             res+=note.note
         return res/len(listeNote)
     return 0

def get_note_user(User_name, id_album):
    listNote = Note.query.join(Note_user).join(User).filter(Note_user.c.Username == User_name).join(Note_table).join(Album).filter(Note_table.c.Album_id == id_album).all()
    if len(listNote) == 0:
        return False
    return int(listNote[len(listNote)-1].note)

def get_Note_user(User_name, id_album):
    listNote = Note.query.join(Note_user).join(User).filter(Note_user.c.Username == User_name).join(Note_table).join(Album).filter(Note_table.c.Album_id == id_album).all()
    if len(listNote) == 0:
        return False
    return listNote

def selectFieldListeGenre(nullValue=False, startWith=False):
    genre = get_genre()
    listeSelectGenre = []
    if nullValue:
        listeSelectRole.append(('null',''))
    if startWith != False:
        listeSelectGenre.append((startWith,startWith))
    for g in genre:
        g = g.name
        if(g != startWith):
            listeSelectGenre.append((g,g))
    return listeSelectGenre

def selectFieldListeRole(nullValue=False, startWith='utilisateur'):
    roles = get_role()
    listeSelectRole = []
    if nullValue:
        listeSelectRole.append(('null',''))
    listeSelectRole.append((startWith,startWith))
    for r in roles:
        r = r.role
        if(r != startWith):
            listeSelectRole.append((r,r))
    return listeSelectRole

def selectFieldListePlaylist(user_name, nullValue=False):
    playlist = get_playlist_user(user_name)
    listeSelectPlaylist = []
    if nullValue:
        listeSelectPlaylist.append(('null',''))
    for p in playlist:
        listeSelectPlaylist.append((p.id,p.name))
    return listeSelectPlaylist
