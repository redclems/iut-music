from src.app import db
from src.models.models import *

def dellet_genre(genre):
    print(genre)
    db.session.delete(genre)
    db.session.commit()

def dellet_album(album):
    db.session.delete(album)
    db.session.commit()

def dellet_author(author):
    db.session.delete(author)
    db.session.commit()

def dellet_parent(parent):
    db.session.delete(parent)
    db.session.commit()

def dellet_user(user):
    db.session.delete(user)
    db.session.commit()

def dellet_playlist(playlist):
    db.session.delete(playlist)
    db.session.commit()

def dellet_album_to_playlist(album, playlist):
    playlist.album.remove(album)
    db.session.commit()
