from src.app import db
from sqlalchemy import Table, Column, Integer, ForeignKey, Date, or_, desc, func, select
from sqlalchemy.orm import relationship
from src.models.models import *
from src.models.get import *
from src.models.new import *

def edit_user(name, password, role):
    user = get_user_exist(name)
    if user != False:

        if role != None:
            user.role_id = get_role_name(role).id

        if password != None:
            user.password = codePasword(password)

        db.session.commit()
        return (("the user %s was edited" % (name)), "alert-success")
    return (("the user %s not exists" % (name)), "alert-danger")

def edit_album(id_album, entryId, title, img, releaseYear, url, idAuthor, idParent, listGenre):
    album = get_album_exist(id_album)
    if album != False:
        if img == None or img == 'null' or img == ''  or img == ' ':
            img = "none.png"
        else:
            album.img = img

        album.entryId = entryId
        album.releaseYear = releaseYear
        album.title = title
        album.url = url
        album.author_id = idAuthor
        album.idParent = idParent

        #ajout des genre
        if listGenre == None:
            listeGenreAlbum = get_genre_album(album.id)
            for g in listeGenreAlbum:
                album.genres.remove(g)
            for g in listGenre:
                g = g.lower()
                gFound = get_genre_exist(g)
                if gFound == False:
                    gFound = new_genre(g)
                if g not in listeGenreAlbum:
                    album.genres.append(gFound)
        db.session.commit()
        return (("the album %s was edited" % (title)), "alert-success")
    return (("the album %s don't exists" % (title)), "alert-danger")

def edit_author(id, name):
    auth = get_author_id(id)
    par = get_parent_id(id)
    if auth != False:
        auth.name = name
        db.session.commit()
        if par != False:
            auth.name = name
            db.session.commit()
            return (("the author %d was edited" % (id)), "alert-success")
        return (("the author %d was edited but don't change the parent" % (id)), "alert-warning")
    return (("the author %d not exists" % (id)), "alert-danger")

def add_album_PlayList(id_PlayList, id_album):
    print(id_PlayList)
    print(id_album)
    if id_PlayList != "null":
        album = get_album_exist(id_album)
        if album != False:
            o = get_playlist_exist(id_PlayList)
            if o != False:
                if len(get_album_in_playlist(id_PlayList, id_album)) == 0:
                    o.album.append(album)
                    db.session.add(album)
                    db.session.commit()
                    return (("the album %s was add to the playlist" % (album.title)), "alert-success")
                return (("the album %s is already in playlist" % (album.title)), "alert-warning")
            return (("the playlist %d not exists" % (id_PlayList)), "alert-danger")
        return (("the album %d not exists" % (id_album)), "alert-danger")
    return ("an error occurred", "alert-danger")
