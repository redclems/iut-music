import click
from src.app import app, db
from src.models.models import *
from src.models.new import *

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    ''' Create the tables and populates the with data'''

    #création de toutes les tables
    db.create_all()

    #chargement de notre jeu de donées
    import yaml
    album = yaml.load(open(filename), Loader=yaml.SafeLoader)

    #import des modèles

    #premier passe: creation de tous les auteurs qui sont copier dans la table parent
    authors = {}
    for b in album:
        a = b["by"]
        if a not in authors:
            authors[a] = new_author(a)

    #premier/demi passe: creation de tous les parents
    parent = {}
    for b in album:
        p = b["parent"]
        if p not in parent:
            if p not in authors:
                authors[p] = new_author(p)
            parent[p] = new_parent(authors[p].id, p)

    #deuxieme passe: création de tous les albums
    albums = {}
    for b in album:
        o = new_album(b["entryId"],
                    b["title"],
                    b["img"],
                    b["releaseYear"],
                    b["url"],
                    authors[b["by"]].id,
                    parent[b["parent"]].id,
                    b["genre"])

        albums[a] = o
