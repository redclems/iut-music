import click
from src.app import app, db

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    '''create new user'''
    from src.models.models import User
    from src.models.new import new_user

    new_user(username, password)