import click
from src.app import app, db

@app.cli.command()
@click.argument('filename')


def loaduser(filename):
    ''' Create the tables and populates the with data'''

    #création de toutes les tables
    db.create_all()

    #chargement de notre jeu de donées
    import yaml
    user = yaml.load(open(filename), Loader=yaml.SafeLoader)

    #import des modèles
    from src.models.models import User, Role
    from src.models.new import new_role, new_user

    #premier passe: creation de tous les roles
    role = {}
    for b in user:
        r = b["role"]
        if r not in role:
            role[r] = new_role(r)

    from hashlib import sha256
    #deuxieme passe: creation de tous les users
    for b in user:
        new_user(b["user"], b["pass"], role[b["role"]].id)
